$(document).ready(function() {
	
	
	$("<select />").appendTo("nav.primary");
	
	
	$("<option />", {
		"selected"	: "selected",
		"value"		: "",
		"text"		: "Menu"
		}).appendTo("nav.primary select");
		
		
		$("nav.primary .menu li a").each(function() {
			var el = $(this);
			$("<option />", {
				"value"		: el.attr("href"),
				"text"		: el.text()
				}).appendTo("nav.primary select");
		});

$("nav.primary select").change(function() {
  window.location = $(this).find("option:selected").val();
});

});

