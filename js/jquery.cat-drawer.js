$(document).ready(function() {

	if($('.band.categories h3').hasClass('closed')) {
		$('.band.categories .row').hide();
	} else {
		$('.band.categories .row').show();
		$('.band.categories i.icon-angle-right').removeClass('icon-angle-right').addClass('icon-angle-down');
	};
	
	
	$('.band.categories h3').click(function() {
		if($(this).hasClass('open')) {
					$(this).children('i.icon-angle-down').removeClass('icon-angle-down').addClass('icon-angle-right');  
		            $(this).siblings('.row').slideUp('normal',function() {  
		                $(this).prev().removeClass('open').addClass('closed');  
		            });  
		        } else {
		        	$(this).children('i.icon-angle-right').removeClass('icon-angle-right').addClass('icon-angle-down');  
		            $(this).siblings('.row').slideToggle('normal',function() {  
		                            $(this).prev().toggleClass('open').addClass('closed');  
		            }); 
		        }  
		        return false;
	});
});

